declare var _default: (newUrlBase: string, oldUrl: string, pathOldUrlIsRelativeTo?: string) => string;
export default _default;
